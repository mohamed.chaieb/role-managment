variable "region" {
  type = string
}
variable "cluster_name" {
  type = string
}
variable "eks_version" {
  type = string
}
variable "cidr" {
  type = string
}
variable "cidr_blocks" {
  type = string
}
variable "application_name" {
  type = string
}
