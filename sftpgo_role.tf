data "aws_s3_bucket" "golden-recipe-data" {
  bucket = "golden-recipe-data"
}
data "aws_s3_bucket" "document-management-system-bucket" {
  bucket = "document-management-system-bucket"
}
data "aws_s3_bucket" "learning-models-bucket" {
  bucket = "learning-models-bucket"
}
data "aws_s3_bucket" "golden-recipies-pipeline" {
  bucket = "golden-recipies-pipeline"
}
data "aws_s3_bucket" "family-office-data" {
  bucket = "family-office-data"
}
data "aws_s3_bucket" "dnd-staging-data" {
  bucket = "dnd-staging-data"
}
data "aws_kms_key" "cmk" {
  key_id = "alias/CypherSnowKey"
}
data "aws_kms_key" "kms" {
  key_id = "alias/ProductFactoryKmsKey"
}
data "aws_s3_bucket" "sftpgo" {
  bucket ="sftpgo-eu-central-1-lesaffre"
}
resource "aws_iam_policy" "sftpgo_role" {
  name        = "sftpgo_s3_access_policy"
  path        = "/"
  description = "s3 bucket access"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Action" : [
          "s3:ListBucket",
          "s3:GetBucketLocation",
          "s3:ListBucketMultipartUploads"
        ],
        "Effect" : "Allow",
        "Resource" : [
          "${data.aws_s3_bucket.sftpgo.arn}",
          "${data.aws_s3_bucket.document-management-system-bucket.arn}",
          "${data.aws_s3_bucket.golden-recipe-data.arn}",
          "${data.aws_s3_bucket.golden-recipies-pipeline.arn}",
          "${data.aws_s3_bucket.learning-models-bucket.arn}",
          "${data.aws_s3_bucket.family-office-data.arn}",
          "${data.aws_s3_bucket.dnd-staging-data.arn}"
        ]
      },
      {
        "Action" : [
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject",
          "s3:ListMultipartUploadParts",
          "s3:AbortMultipartUpload"
        ],
        "Effect" : "Allow",
        "Resource" : [
          "${data.aws_s3_bucket.sftpgo.arn}/*",
          "${data.aws_s3_bucket.document-management-system-bucket.arn}/*",
          "${data.aws_s3_bucket.golden-recipe-data.arn}/*",
          "${data.aws_s3_bucket.golden-recipies-pipeline.arn}/*",
          "${data.aws_s3_bucket.learning-models-bucket.arn}/*",
          "${data.aws_s3_bucket.family-office-data.arn}/*",
          "${data.aws_s3_bucket.dnd-staging-data.arn}/*"
        ]

      },
      {
        Action = [
          "kms:Decrypt",
          "kms:GenerateDataKey",
          "kms:Encrypt",

        ],
        Effect   = "Allow",
        Resource = ["${data.aws_kms_key.kms.arn}", "${data.aws_kms_key.cmk.arn}"]
      }
    ]
  })
  tags = merge(local.tags, {
    cluster = var.cluster_name
    Name    = "sftpgo"
  })
}

data "aws_eks_cluster" "cluster" {
  name = var.cluster_name
}
data "aws_iam_openid_connect_provider" "cluster_oidc" {
  url = data.aws_eks_cluster.cluster.identity.0.oidc.0.issuer
}

module "sftpgo_role" {
  
  force_detach_policies = false
  source = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name = "sftpgo_role"

  role_policy_arns = {
    policy = "${aws_iam_policy.sftpgo_role.arn}"
  }

  oidc_providers = {

    main = {
      provider_arn               = data.aws_iam_openid_connect_provider.cluster_oidc.arn
      namespace_service_accounts = ["sftpgo:sftpgo-sa"]
    }

  }

  tags = merge(local.tags, {
    cluster = var.cluster_name
    Name    = "sftpgo"

  })

}

locals {
  tags = {
    creator = "walid"
    project = "productFactory"
    team    = "All"
  }
}
