
terraform {
  backend "s3" {
    bucket  = "lesaffre-tfstates"
    key     = "eu-central-1/productFactory/roleManegement/terraform.tfstate"
    region  = "eu-west-3"
    profile = "wless"
  }
}